﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Net;
using Android.Widget;
using Android.OS;
using Android.Provider;

namespace ReAl.Xamarin.Lab05.Andriod
{
    [Activity(Label = "Phone App", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText txtNumero;
        private Button btnConvertir;
        private Button btnLlamar;
        private Button btnHistorial;
        private Button btnValidar;
        
        static List<string> phoneNumbers = new List<string>();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            txtNumero = FindViewById<EditText>(Resource.Id.txtPhoneNumber);
            btnConvertir = FindViewById<Button>(Resource.Id.btnConvertir);
            btnLlamar = FindViewById<Button>(Resource.Id.btnLlamar);
            btnHistorial = FindViewById<Button>(Resource.Id.btnHistorial);
            btnValidar = FindViewById<Button>(Resource.Id.btnValidar);

            btnLlamar.Enabled = false;
            var strNumeroIntroducido = string.Empty;

            btnConvertir.Click += (sender, args) =>
            {
                var miTraductor = new CPhoneTraslator();
                strNumeroIntroducido = miTraductor.strToNumber(txtNumero.Text);

                if (string.IsNullOrWhiteSpace(strNumeroIntroducido))
                {
                    btnLlamar.Text = "Llamar";
                    btnLlamar.Enabled = false;
                }
                else
                {
                    btnLlamar.Text = "Llamar al " + strNumeroIntroducido;
                    btnLlamar.Enabled = true;
                }
            };

            btnLlamar.Click += (sender, args) =>
            {
                //Llamamos
                var callDialog = new AlertDialog.Builder(this);
                callDialog.SetMessage($"Llamar al numero {strNumeroIntroducido}?");
                callDialog.SetNeutralButton("Llamar", delegate(object o, DialogClickEventArgs eventArgs)
                {
                    //Guardamos el registro en el array
                    phoneNumbers.Add(strNumeroIntroducido);
                    btnHistorial.Enabled = true;

                    //Crear un Intent
                    var callIntent = new Android.Content.Intent(Intent.ActionCall);
                    callIntent.SetData(Uri.Parse($"tel:{strNumeroIntroducido}"));
                    StartActivity(callIntent);
                });
                callDialog.SetNegativeButton("Cancelar", delegate(object o, DialogClickEventArgs eventArgs) { });
                callDialog.Show();
            };

            btnHistorial.Click += (sender, args) =>
            {
                var Intent = new Intent(this, typeof(HistorialActivity));
                Intent.PutStringArrayListExtra("phone_numbers", phoneNumbers);
                StartActivity(Intent);
            };

            btnValidar.Click += (sender, args) =>
            {
                var Intent = new Intent(this, typeof(ValidarActivity));                
                StartActivity(Intent);
            };
        }

    }
}

