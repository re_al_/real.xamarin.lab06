﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ReAl.Xamarin.Lab05.Andriod
{
    [Activity(Label = "Validar Actividad", Icon = "@drawable/icon")]
    public class ValidarActivity : Activity
    {
        private EditText txtUsuario;
        private EditText txtPassword;
        private Button btnValidar;
        private TextView txtMensaje;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView (Resource.Layout.Validar);

            txtUsuario = FindViewById<EditText>(Resource.Id.txtCorreo);
            txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            btnValidar = FindViewById<Button>(Resource.Id.btnValidar);
            txtMensaje = FindViewById<TextView>(Resource.Id.txtMensaje);

            btnValidar.Click += (sender, args) =>
            {
                Validate();
            };
        }

        private async void Validate()
        {
            var client = new SALLab06.ServiceClient();
            var miCorreo = txtUsuario.Text;
            var miPass = txtPassword.Text;
            var miDeviceId = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            SALLab06.ResultInfo resultado = await client.ValidateAsync(miCorreo, miPass, miDeviceId);

            txtMensaje.Text = resultado.Status + "\r\n\r\n" + resultado.Fullname + "\r\n" + resultado.Token;
        }

    }
}